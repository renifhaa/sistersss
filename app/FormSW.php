<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormSW extends Model
{
    protected $table = "forms_sw";

    protected $guarded = ["id"];

}
