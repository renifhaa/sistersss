<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FormSW;

class FormSWController extends Controller
{
    public function create()
    {
        return view('layouts.inputsw');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'published' => 'required',
            'happened' => 'required',
            'kbg' => 'required',
            'age' => 'required',
            'domicile' => 'required',
            'story' => 'required',
        ]);

        FormSW::create($validated);

        return redirect()->back()->with('status', 'Data Successfully Added.');
    }

    public function index()
    {
        $allSw = FormSW::all();
        return view('admin.sw.submission', compact('allSw'));
    }
}
