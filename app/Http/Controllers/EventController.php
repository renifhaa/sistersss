<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use DB;
use File;

class EventController extends Controller
{
    public function create()
    {
        return view('admin.event.add');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'picture' => 'required|mimes:jpeg,jpg,png|max:2200',
            'link' => 'required',
            'enddate' => 'required|date'
        ]);

        $picture = $request->picture;
        $new_picture = time() . ' - ' . $picture->getClientOriginalName();


        Event::create([
            "title" => $request["title"],
            "description" => $request["description"],
            "picture" => $new_picture,
            "link" => $request["link"],
            "enddate" => date('Y-m-d',strtotime($request["enddate"]))

        ]);

        $picture->move('img-upload/', $new_picture);

        return redirect('/admin/list-event')->with('success', 'Data submission successful!');
    }

    public function index()
    {
        $events = Event::all();
        return view('admin.event.list', compact('events'));
    }
    
    public function edit($id) {
        $event = Event::find($id);
        return view('admin.event.edit',compact('event'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'picture' => 'mimes:jpeg,jpg,png|max:2200',
            'link' => 'required',
            'enddate' => 'required|date'
        ]);

        $event = Event::findorfail($id);
        if ($request->has('picture')) {
            File::delete("img-upload/".$event->picture);
            $picture = $request->picture;
            $new_picture = time() . ' - ' . $picture->getClientOriginalName();
            $picture->move('img-upload/', $new_picture);
            $event_data = [
                "title" => $request["title"],
                "description" => $request["description"],
                "picture" => $new_picture,
                "link" => $request["link"],
                "enddate" => date('Y-m-d',strtotime($request["enddate"]))
            ];
        } else {
            $event_data = [
                "title" => $request["title"],
                "description" => $request["description"],
                "link" => $request["link"],
                "enddate" => date('Y-m-d',strtotime($request["enddate"]))
            ];
        }
        
        $event->update($event_data);

        return redirect('/admin/list-event')->with('success', 'Submission successfully updated!');
    }

    public function destroy($id) {
        $submission = DB::table('events')->where('id', $id)->delete();
        return redirect('/admin/list-event')->with('success', 'Submission successfully deleted!');
    }

    public function upload($id) {
        $event = Event::where('id',$id)->first();
        return view('layouts.event_content',compact('event')); 
    }

    public function list_event() {
        $events = Event::latest()->paginate(8);
        $carousels = Event::latest()->limit(3)->get();

        return view('layouts.events',compact('events','carousels')); 
    }
}
