@extends('layouts.master')

@section('content')
    <!-- Events -->

    <section
      class="container"
      style="width: 90%; margin: 100px auto; text-align: center"
    >
      <h4 class="mb-5"><strong>UPCOMING EVENTS</strong></h4>

      <div class="row">
        @foreach ($events as $event)
        <div class="col-lg-4 col-md-12 mb-4">
          <div class="card" >
            <div
              class="bg-image hover-overlay ripple"
              data-mdb-ripple-color="light"
            >
              <img src="{{asset('img-upload/'.$event->picture)}}" class="img-fluid" />
            </div>
            <div class="card-body">
              <h4 class="card-title">{{$event->title}}</h4>
              <p class="card-text">
                {!!$event->description!!}
              </p>
              <a
                href="{{$event->link}}"
                target="__blank"
                class="btn btn-primary"
                style="margin-top: 20px"
                >Sign Up</a
              >
            </div>
          </div>
        </div>
        @endforeach

        
      </div>
    </section>

    <!-- Last Events -->

    <section
      class="container"
      style="width: 90%; margin: 100px auto; text-align: center"
    >
      <h4 class="mb-5"><strong>OUR PAST EVENTS</strong></h4>

      <div class="row">
        <div class="col-lg-4 col-md-12 mb-4">
          <div class="card">
            <div
              class="bg-image hover-overlay ripple"
              data-mdb-ripple-color="light"
            >
              <img src="{{asset('/layouts/img/sistercamp.jpeg')}}" class="img-fluid" />
            </div>
            <div class="card-body">
              <h4 class="card-title">SISTERCAMP: SPEAK UP YOUR SELF</h4>
              <p class="card-text">
                TOPICS: - We want to hear from you - If I can do it, you can too
                - You are your own brand - Speak the untold truth
              </p>
              <a
                href="https://bit.ly/SISTERCAMP"
                class="btn btn-primary"
                style="margin-top: 20px"
                >Highlight Event</a
              >
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 mb-4">
          <div class="card">
            <div
              class="bg-image hover-overlay ripple"
              data-mdb-ripple-color="light"
            >
              <img src="{{asset('/layouts/img/womenxfashioncamp.jpg')}}" class="img-fluid" />
            </div>
            <div class="card-body">
              <h4 class="card-title">WomenXFashion : "Old But Gold"</h4>
              <p class="card-text">
                A campaign to encourage sisters to cultivate a culture of thrift shopping.
              </p>
              <a href="https://bit.ly/WOMENXFASHIONCAMPAIGN" class="btn btn-primary" style="margin-top: 20px"
                >Highlight Event</a
              >
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 mb-4">
          <div class="card">
            <div
              class="bg-image hover-overlay ripple"
              data-mdb-ripple-color="light"
            >
              <img src="{{asset('/layouts/img/thriftshop.jpg')}}" class="img-fluid" />
              <a href="#!">
                <div
                  class="mask"
                  style="background-color: rgba(251, 251, 251, 0.15)"
                ></div>
              </a>
            </div>
            <div class="card-body">
              <h4 class="card-title">Open Booth Thrift Shop Owners</h4>
              <p class="card-text">
                Come and join us at Women X Fashion Campaign to promote your thrift shop🥳
              </p>
              <a href="https://bit.ly/BOOTHREGISTRATION" class="btn btn-primary" style="margin-top: 20px"
                >Highlight Event</a
              >
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection