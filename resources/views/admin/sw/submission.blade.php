@extends('admin.master')

@section('title')
    List Submission
@endsection

@section('content')
    @if(session('success')) 
        <div class="alert alert-success">
            {{session('success')}}
        </div>
    @endif
    
    <table class="table">
        <thead class="thead-light">
            <tr>
            <th scope="col">#</th>
            <th scope="col">Published</th>
            <th scope="col">Happened in</th>
            <th scope="col">KBG Type</th>
            <th scope="col">Age</th>
            <th scope="col">Domicile</th>
            <th scope="col" width="300px">Story</th>
            <th scope="col" >Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($allSw as $key=>$sw)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$sw->published}}</td>
                    <td>{{$sw->happened}}</td>
                    <td>{{$sw->kbg}}</td>
                    <td>{{$sw->age}}</td>
                    <td>{{$sw->domicile}}</td>
                    <td>{{ Str::limit($sw->story, 50) }}</td>
                    <td>
                        <form action="/admin/suarawanita/{{$sw->id}}" method="POST">
                            <a href="/admin/suarawanita/{{$sw->id}}" class="btn btn-info">Edit</a>
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" onclick="return confirm('Are you sure?')" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td class="text-center">No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
@endsection