@extends('admin.master')

@section('title')
    List Submission
@endsection

@section('content')
    @if(session('success')) 
        <div class="alert alert-success">
            {{session('success')}}
        </div>
    @endif

    <a href="/admin/add-event" class="btn btn-primary mb-3">Add New Event</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col"width="300px">Description</th>
            <th scope="col"width="150px">Picture</th>
            <th scope="col">Link</th>
            <th scope="col">End Date</th>
            <th scope="col">Date Created</th>
            <th scope="col" >Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($events as $key=>$event)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$event->title}}</td>
                    <td>{{Str::limit($event->description, 60)}}</td>
                    <td>{{$event->picture}}</td>
                    <td>{{$event->link}}</td>
                    <td>{{$event->enddate}}</td>
                    <td>{{$event->created_at}}</td>
                    <td>
                        <form action="/admin/event/{{$event->id}}" method="POST">
                            <a href="/admin/event/{{$event->id}}" class="btn btn-info">Edit</a>
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" onclick="return confirm('Are you sure?')" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td class="text-center">No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
@endsection