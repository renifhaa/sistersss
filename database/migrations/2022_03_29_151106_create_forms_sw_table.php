<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsSwTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms_sw', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('published',20);
            $table->string('happened',20);
            $table->string('kbg',30);
            $table->string('age',20);
            $table->string('domicile',50);
            $table->longText('story');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms_sw');
    }
}
